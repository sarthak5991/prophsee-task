// var scripts = document.getElementsByTagName("script")
// var currentScriptPath = scripts[scripts.length-1].src;

app.directive('showIllus', function() {
	console.log('inside illustrations-show directive');
	return {
		restrict: 'E',
		template: "<illus-tration> </illus-tration>",
		replace: true
	};
});

app.directive('illusTration', function($templateRequest, $compile) {
	console.log('inside illus-trations directive');
		return function(scope, element, attrs) {
			$templateRequest('app/directives/illustrations.html').then(function(html) {
				var template = angular.element(html);
				element.append(template);
				$compile(template)(scope);
			});
		};
	
});