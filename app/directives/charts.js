// var scripts = document.getElementsByTagName("script")
// var currentScriptPath = scripts[scripts.length-1].src;

app.directive('chartShow', function() {
	console.log('inside chartShow Directive');
	return {
		restrict: 'E',
		template: "<chart-data> </chart-data>",
		replace: true
	};
});

app.directive('chartData', function($templateRequest, $compile) {
		return function(scope, element, attrs) {
			$templateRequest('app/directives/charts.html').then(function(html) {
				var template = angular.element(html);
				element.append(template);
				$compile(template)(scope);
			});
		};
	
});
