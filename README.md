Completed the PropheSee Task of creating charts through D3.js and HighCharts API an illustrations.     
Used AngularJS, d3.js, highCharts, Sementic-UI, jquery  
   
Installation -   
  1) Clone Repository(or download zip)  
  2) Install 'http-server' npm package
         
```
#!install

npm install http-server     
```   
3) Go to root directory of project and run 'http-server' command      
  

The Site is Hosted Here - http://prophesee-task.bitballoon.com